﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace BaseDeDatosEscuela
{
    class conexion
    {
        private static conexion instance = new conexion();
        private SqlConnection conexin;

        private conexion()
        {
            conexin = new SqlConnection(Properties.Settings.Default.conexion);
            conexin.Open();
        }
    }
}
