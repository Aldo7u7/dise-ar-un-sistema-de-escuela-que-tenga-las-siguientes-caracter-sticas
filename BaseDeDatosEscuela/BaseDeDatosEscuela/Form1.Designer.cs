﻿namespace BaseDeDatosEscuela
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Alumnos = new System.Windows.Forms.Button();
            this.profesores = new System.Windows.Forms.Button();
            this.materias = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(97, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "INSTITUTO TECNOLOGICO DE TIJUANA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(166, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Base de Datos General";
            // 
            // Alumnos
            // 
            this.Alumnos.Location = new System.Drawing.Point(186, 130);
            this.Alumnos.Name = "Alumnos";
            this.Alumnos.Size = new System.Drawing.Size(75, 23);
            this.Alumnos.TabIndex = 2;
            this.Alumnos.Text = "Alumnos";
            this.Alumnos.UseVisualStyleBackColor = true;
            this.Alumnos.Click += new System.EventHandler(this.Alumnos_Click);
            // 
            // profesores
            // 
            this.profesores.Location = new System.Drawing.Point(186, 86);
            this.profesores.Name = "profesores";
            this.profesores.Size = new System.Drawing.Size(75, 23);
            this.profesores.TabIndex = 3;
            this.profesores.Text = "Docentes";
            this.profesores.UseVisualStyleBackColor = true;
            this.profesores.Click += new System.EventHandler(this.profesores_Click);
            // 
            // materias
            // 
            this.materias.Location = new System.Drawing.Point(186, 174);
            this.materias.Name = "materias";
            this.materias.Size = new System.Drawing.Size(75, 22);
            this.materias.TabIndex = 4;
            this.materias.Text = "Materias";
            this.materias.UseVisualStyleBackColor = true;
            this.materias.Click += new System.EventHandler(this.materias_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::BaseDeDatosEscuela.Properties.Resources.logo_ITT1;
            this.pictureBox1.Location = new System.Drawing.Point(143, 225);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(170, 127);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 389);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.materias);
            this.Controls.Add(this.profesores);
            this.Controls.Add(this.Alumnos);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Alumnos;
        private System.Windows.Forms.Button profesores;
        private System.Windows.Forms.Button materias;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

