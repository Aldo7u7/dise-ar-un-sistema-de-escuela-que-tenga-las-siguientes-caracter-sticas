﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseDeDatosEscuela;
namespace BaseDeDatosEscuela
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Alumnos_Click(object sender, EventArgs e)
        {
            Form estufiante = new alumnos();
            estufiante.Show();
        }

        private void profesores_Click(object sender, EventArgs e)
        {
            Form profe = new profesores();
            profe.Show();
        }

        private void materias_Click(object sender, EventArgs e)
        {
            Form asignaturas = new materias();
            asignaturas.Show();
        }
    }
}
