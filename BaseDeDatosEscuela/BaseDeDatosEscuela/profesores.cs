﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace BaseDeDatosEscuela
{
    public partial class profesores : Form
    {
        public profesores()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void profesores_Load(object sender, EventArgs e)
        {

            actualizarDatos();
        }

        private void actualizarDatos()
        {
            // TODO: This line of code loads data into the 'dataDocentes.docente' table. You can move, or remove it, as needed.
            this.docenteTableAdapter1.Fill(this.dataDocentes.docente);
            // TODO: This line of code loads data into the 'identidadesDocente.docente' table. You can move, or remove it, as needed.
            this.docenteTableAdapter.Fill(this.identidadesDocente.docente);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand(" Insert into Docente (id_docente,nombre,apellido,fecha_nacimiento,identidad,contrasena) values (@nombre,@apellido,@fecha_nacimiento,@identidad,@contrasena)  ");
            cmd.Parameters.Add("@nombre:", pantalla1.Text);
            cmd.Parameters.Add("@apellido:", pantalla2.Text);
            cmd.Parameters.Add("@identidad:", pantalla3.Text);
            cmd.Parameters.Add("@contrasena:", pantalla4.Text);
            cmd.Parameters.Add("@fecha_nacimiento:", dateTimePicker1.Value);

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show(" **Docente Actualizado Satisfactoriamente** ");
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("update Docente set nombre=@nombre,apellido=@apellido,fecha_nacimiento=@fecha_nacimiento,identidad=@identidad,contrasena=@contrasena where id_docente=@id");
            cmd.Parameters.Add("@nombre:", pantalla1.Text);
            cmd.Parameters.Add("@apellido:", pantalla2.Text);
            cmd.Parameters.Add("@identidad:", pantalla3.Text);
            cmd.Parameters.Add("@contrasena:", pantalla4.Text);
            cmd.Parameters.Add("@fecha_nacimiento:", dateTimePicker1.Value);
            cmd.Parameters.Add("@id", comboBox1.Text);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show(" **Docente Eliminado Satisfactoriamente** ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            SqlCommand cmd = new SqlCommand("delete from Docente where id_docente=@id");
            cmd.Parameters.Add("@id", comboBox1.Text);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show(" **Docente Eliminado Satisfactoriamente** ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
